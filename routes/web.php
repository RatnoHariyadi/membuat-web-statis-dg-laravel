<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*	Membuat Web Statis dg Laravel

Route::get('/', function () {
    return view('home--');
});

Route::get('/register', function () {
    return view('register--');
});

Route::get('/welcome', function () {
    return view('welcome--');
});

Route::get('/', 'HomeController@index')->name('home');
Route::get('/register', 'AuthController@register');
Route::get('/welcome', 'AuthController@welcome');
Route::post('/welcome', 'AuthController@store');

*/


/*	Memasangkan Template dg Laravel Blade

Route::get('/', 'HomeController@index')->name('home');

Route::get('/data-tables', function () {
    return view('data-tables');
});

*/

//	Berlatih CRUD di Laravel

Route::get('/', function () {
    return view('create');
});

Route::get('/data-tables', function () {
    return view('data-tables');
});



Route::get('/pertanyaan', 'PertanyaanController@index');
Route::get('/pertanyaan/create', 'PertanyaanController@create');
Route::post('/pertanyaan', 'PertanyaanController@store');
Route::get('/pertanyaan/{pertanyaan_id}', 'PertanyaanController@show');
Route::get('/pertanyaan/{pertanyaan_id}/edit', 'PertanyaanController@edit');
Route::put('/pertanyaan/{pertanyaan_id}', 'PertanyaanController@update');
Route::delete('/pertanyaan/{pertanyaan_id}', 'PertanyaanController@destroy');





	
