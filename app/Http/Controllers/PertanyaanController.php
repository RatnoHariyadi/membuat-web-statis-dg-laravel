<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class PertanyaanController extends Controller
{
    public function index(){
    	return view('index');
    }

    public function create(){
    	return view('create');
    }

    public function store(Request $request){
    	//dd ($request->all());
    	$query = DB::table('pertanyaan')->insert([
    		"judul" => $request["judul"],
    		"isi" => $request["isi"]
	    ]);
	
	    return redirect('/pertanyaan')->with('succes', 'Post Berhasil Disimpan!');
    }
    public function index(){
    	$pertanyaan = DB::table('pertanyaan')->get();
    	return view ('index', compact('pertanyaan'));
    }
    public function show($pertanyaan_id){
    	$post = DB::table('pertanyaan')->where('id', $pertanyaan_id')->first();	
    	return view('show', compact('post'));
    }
    public function edit($pertanyaan_id){
    	$post = DB::table('pertanyaan')->where('id', $pertanyaan_id')->first();	
    	return view('edit', compact('post'));
    }
}
